package br.edu.ufabc.pdm.numericalcalculus.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.ufabc.pdm.numericalcalculus.activities.Bisection;
import br.edu.ufabc.pdm.numericalcalculus.activities.FixedPoint;
import br.edu.ufabc.pdm.numericalcalculus.activities.Gauss;
import br.edu.ufabc.pdm.numericalcalculus.activities.MidPoint;
import br.edu.ufabc.pdm.numericalcalculus.activities.NewtonRaphson;
import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.activities.Secant;
import br.edu.ufabc.pdm.numericalcalculus.activities.Simpson;
import br.edu.ufabc.pdm.numericalcalculus.activities.Trapeze;


public class ResultFragment extends Fragment {

    private static final String LOGTAG = ResultFragment.class.getSimpleName();

    private Double result;
    private ArrayList<Double> iteractions;
    public View view;
    private String function;
    private String method;
    private String numbers;
    private int repeat;
    private int type;
    public int APROXIMATION = 1;
    public int INTEGRATION = 2;

    public void setup(double result, ArrayList<Double> iteractions, String function, String method, int repeat, String numbers)  {
        this.result = result;
        this.iteractions = iteractions;
        this.function = function;
        this.method = method;
        this.numbers = numbers;
        this.repeat = repeat;
        if (this.method.equals(Bisection.class.getSimpleName()) || this.method.equals(FixedPoint.class.getSimpleName()) || this.method.equals(NewtonRaphson.class.getSimpleName()) || this.method.equals(Secant.class.getSimpleName())) {
            type = APROXIMATION;
        } else if(this.method.equals(MidPoint.class.getSimpleName()) || this.method.equals(Trapeze.class.getSimpleName()) || this.method.equals(Simpson.class.getSimpleName())) {
            type = INTEGRATION;
        }else if (this.method.equals(Gauss.class.getSimpleName()));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.result_fragment, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        TextView label = (TextView )view.findViewById(R.id.result_label);
        TextView result = (TextView )view.findViewById(R.id.result_root);
        TextView method = (TextView )view.findViewById(R.id.result_method);
        TextView function = (TextView )view.findViewById(R.id.result_function);
        TextView numbers = (TextView )view.findViewById(R.id.result_numbers);
        TextView repeat = (TextView )view.findViewById(R.id.result_repeat);

        TextView iteractions = (TextView )view.findViewById(R.id.result_iteractions);
        result.setText(String.valueOf(this.result));
        method.setText(this.method);
        function.setText(this.function);
        numbers.setText(this.numbers);
        if(this.type == APROXIMATION) {
            repeat.setText(this.repeat+" repeticões");
            label.setText("Raíz aproximada:");
        } else if(this.type == INTEGRATION) {
            repeat.setText(this.repeat+" intervalos");
            label.setText("Integral aproximada:");
        }
        if (this.iteractions != null) {
            for (int i = 0; i < this.iteractions.size(); i++) {
                int n = i+1;
                iteractions.append("Iteracão "+n+": "+this.iteractions.get(i)+"\n");
            }
        }
    }
}
