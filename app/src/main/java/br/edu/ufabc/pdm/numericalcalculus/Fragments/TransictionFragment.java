package br.edu.ufabc.pdm.numericalcalculus.Fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import br.edu.ufabc.pdm.numericalcalculus.R;

/**
 * Created by Felipe on 30/04/2015.
 */
public class TransictionFragment extends Fragment {

    private static final String LOGTAG = TransictionFragment.class.getSimpleName();
    private String method;
    private SystemFragment systemFragment;
    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.transiction_fragment, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        Button button = (Button) view.findViewById(R.id.system2x2);
        Button button1 = (Button) view.findViewById(R.id.system3x3);
        //Button button2 = (Button) view.findViewById(R.id.system4x4);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                systemFragment = new SystemFragment();
                systemFragment.setMethod(method);
                systemFragment.setSize(SystemFragment.M2);
                FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
                transaction1.replace(R.id.method_container, systemFragment);
                transaction1.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction1.commit();
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                systemFragment = new SystemFragment();
                systemFragment.setMethod(method);
                systemFragment.setSize(SystemFragment.M3);
                FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
                transaction1.replace(R.id.method_container, systemFragment);
                transaction1.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction1.commit();
            }
        });
    }
}