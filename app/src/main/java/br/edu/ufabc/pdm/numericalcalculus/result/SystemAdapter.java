package br.edu.ufabc.pdm.numericalcalculus.result;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.Fragments.SystemFragment;
import br.edu.ufabc.pdm.numericalcalculus.singleton.SystemsDao;

/**
 * Created by eduardo on 30/04/15.
 */
public class SystemAdapter extends BaseAdapter {
    private SystemsDao dao;
    private ArrayList<SystemResult> results;
    private Context context;
    private LayoutInflater inflater;

    public SystemAdapter(Context c) {
        this.context = c;
        dao = SystemsDao.instantiate(context);
        results = dao.list();
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Object getItem(int position) {
        return results.get(position);
    }

    @Override
    public long getItemId(int position) {
        return results.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        TextView a11;
        TextView a12;
        TextView a13;
        TextView a21;
        TextView a22;
        TextView a23;
        TextView a31;
        TextView a32;
        TextView a33;
        TextView b1;
        TextView b2;
        TextView b3;
        if (convertView == null) {
            view = inflater.inflate(R.layout.history_item_matrix, null);
        } else
            view = convertView;

        SystemResult result = results.get(position);
        a11 = (TextView )view.findViewById(R.id.a11);
        a12 = (TextView )view.findViewById(R.id.a12);
        a13 = (TextView )view.findViewById(R.id.a13);
        a21 = (TextView )view.findViewById(R.id.a21);
        a22 = (TextView )view.findViewById(R.id.a22);
        a23 = (TextView )view.findViewById(R.id.a23);
        a31 = (TextView )view.findViewById(R.id.a31);
        a32 = (TextView )view.findViewById(R.id.a32);
        a33 = (TextView )view.findViewById(R.id.a33);
        b1 = (TextView )view.findViewById(R.id.b1);
        b2 = (TextView )view.findViewById(R.id.b2);
        b3 = (TextView )view.findViewById(R.id.b3);

        HashMap<String, Double> coefs = result.getCoefs();
        a11.setText(String.valueOf(coefs.get("a11")));
        a12.setText(String.valueOf(coefs.get("a12")));
        a21.setText(String.valueOf(coefs.get("a21")));
        a22.setText(String.valueOf(coefs.get("a22")));
        b1.setText(String.valueOf(coefs.get("b1")));
        b2.setText(String.valueOf(coefs.get("b2")));

        if (result.getType() == SystemFragment.M2) {
            a13.setVisibility(View.GONE);
            a23.setVisibility(View.GONE);
            a31.setVisibility(View.GONE);
            a32.setVisibility(View.GONE);
            a33.setVisibility(View.GONE);
            b3.setVisibility(View.GONE);
        } else if (result.getType() == SystemFragment.M3) {
            a13.setText(String.valueOf(coefs.get("a13")));
            a23.setText(String.valueOf(coefs.get("a23")));
            a31.setText(String.valueOf(coefs.get("a31")));
            a32.setText(String.valueOf(coefs.get("a32")));
            a33.setText(String.valueOf(coefs.get("a33")));
            b3.setText(String.valueOf(coefs.get("b3")));
        }

        return view;
    }
}
