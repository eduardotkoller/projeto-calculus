package br.edu.ufabc.pdm.numericalcalculus.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.HashMap;

import br.edu.ufabc.pdm.numericalcalculus.activities.Gauss;
import br.edu.ufabc.pdm.numericalcalculus.Fragments.SystemFragment;
import br.edu.ufabc.pdm.numericalcalculus.result.SystemResult;
import br.edu.ufabc.pdm.numericalcalculus.singleton.SystemsDao;

/**
 * Created by Felipe on 30/04/2015.
 */
public class SystemService extends IntentService {

    private SystemsDao dao;


    public static final String BROADCAST_ACTION = SystemService.class.getPackage() + ".DONE";

    public SystemService() {
        super("SystemService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        dao = SystemsDao.instantiate(this);
        String method = intent.getExtras().getString("method");
        long type = intent.getExtras().getLong("type");
        Double a11 = intent.getExtras().getDouble("a11");
        Double a12 = intent.getExtras().getDouble("a12");
        Double a13 = intent.getExtras().getDouble("a13");
        Double a21 = intent.getExtras().getDouble("a21");
        Double a22 = intent.getExtras().getDouble("a22");
        Double a23 = intent.getExtras().getDouble("a23");
        Double a31 = intent.getExtras().getDouble("a31");
        Double a32 = intent.getExtras().getDouble("a32");
        Double a33 = intent.getExtras().getDouble("a33");
        Double b1 = intent.getExtras().getDouble("b1");
        Double b2 = intent.getExtras().getDouble("b2");
        Double b3 = intent.getExtras().getDouble("b3");

        HashMap<String, Double> coefs = new HashMap<String, Double>();
        coefs.put("a11", a11);
        coefs.put("a12", a12);
        coefs.put("a13", a13);
        coefs.put("a21", a21);
        coefs.put("a22", a22);
        coefs.put("a23", a23);
        coefs.put("a31", a31);
        coefs.put("a32", a32);
        coefs.put("a33", a33);
        coefs.put("b1", b1);
        coefs.put("b2", b2);
        coefs.put("b3", b3);

        SystemResult resultObj = new SystemResult();
        resultObj.setCoefs(coefs);
        Double result[] = new Double[] {};
        if (method.equals(Gauss.class.getSimpleName())) {
            if (type== SystemFragment.M2) {
                result = gauss(a11, a12, a21, a22, b1, b2);
            } else {
                result = gauss3x3(a11, a12, a13, a21, a22, a23, a31, a32, a33, b1, b2, b3);
            }
        }
        resultObj.setType(type);
        resultObj.setResult(result);
        Intent localIntent = new Intent(BROADCAST_ACTION);
        localIntent.putExtra("method", method);
        localIntent.putExtra("result", result);
        localIntent.putExtra("type", type);
        localIntent.putExtra("a11", a11);
        localIntent.putExtra("a12", a12);
        localIntent.putExtra("a21", a21);
        localIntent.putExtra("a22", a22);

        localIntent.putExtra("a13", a13);
        localIntent.putExtra("a23", a23);

        localIntent.putExtra("a31", a31);
        localIntent.putExtra("a32", a32);
        localIntent.putExtra("a33", a33);

        localIntent.putExtra("b1", b1);
        localIntent.putExtra("b2", b2);
        localIntent.putExtra("b3", b3);


        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

        if(dao.add(resultObj))
            Log.w("SYSTEMSERVICE", "added result to database");
        else {
            Log.w("SYSTEMSERVICE", "failed to add result to database");
        }

    }
    private Double[] gauss(Double edit1, Double edit2, Double edit3, Double edit4, Double edit5, Double edit6) {

        Double x2 = (((edit6 - edit5) * (edit1 / edit3)) / ((edit4 * (edit1 / edit3)) - edit2));
        Double x1 = (edit5 - (edit2 * x2)) / edit1;
        Double gauss [] = new Double[]{x2, x1};

        return gauss;
    }
    private Double[] gauss3x3(Double edit1, Double edit2, Double edit3, Double edit4, Double edit5, Double edit6, Double edit7, Double edit8, Double edit9, Double edit10, Double edit11, Double edit12) {

        double a = (edit7*(edit1/edit4))-edit1;
        double b = ((edit8*(edit1/edit4)-edit2)*(edit1/a))-edit2;
        double c = (edit5*(edit1/edit4))-edit2;
        double d = (edit6*(edit1/edit4))-edit3;

        double x3 = ((((((edit12*(edit1/edit4))-edit10)*(edit1/a))-edit10)*(c/b))-d)/(((((edit9*(edit1/edit4))-edit3)*(edit1/a))-edit3)*(b/c))-d;
        double x2 = (((edit11*(edit1/edit4))-edit10)-((edit6*(edit1/edit4))-edit3))/((edit5*(edit1/edit4))-edit2);
        double x1 = (edit10-(x2+x3))/edit1;

        Double gauss3x3 [] = new Double [] {x1,x2,x3};

        return gauss3x3;
    }
}