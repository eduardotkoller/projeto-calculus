package br.edu.ufabc.pdm.numericalcalculus.activities;


import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import br.edu.ufabc.pdm.numericalcalculus.Fragments.HistoryFragment;
import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.singleton.AproximationsDao;
import br.edu.ufabc.pdm.numericalcalculus.singleton.IntegrationsDao;
import br.edu.ufabc.pdm.numericalcalculus.singleton.SystemsDao;


public class History extends ActionBarActivity {

    private IntegrationsDao integrationsDao;
    private AproximationsDao aproximationsDao;
    private SystemsDao systemsDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        HistoryFragment history = new HistoryFragment();
        getFragmentManager().beginTransaction().add(R.id.history_container, history).commit();

        integrationsDao = IntegrationsDao.instantiate(this);
        aproximationsDao = AproximationsDao.instantiate(this);
        systemsDao = SystemsDao.instantiate(this);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_clear_history) {
            boolean i = integrationsDao.clear();
            boolean a = aproximationsDao.clear();
            boolean s = systemsDao.clear();
            HistoryFragment history = new HistoryFragment();
            getFragmentManager().beginTransaction().replace(R.id.history_container, history).commit();
            if (i && a && s) {
                Toast toast = Toast.makeText(this, R.string.clear_successful, Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Toast toast = Toast.makeText(this, R.string.clear_failed, Toast.LENGTH_SHORT);
                toast.show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
