package br.edu.ufabc.pdm.numericalcalculus.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.ArrayList;

import br.edu.ufabc.pdm.numericalcalculus.activities.Bisection;
import br.edu.ufabc.pdm.numericalcalculus.activities.FixedPoint;
import br.edu.ufabc.pdm.numericalcalculus.activities.NewtonRaphson;
import br.edu.ufabc.pdm.numericalcalculus.activities.Secant;
import br.edu.ufabc.pdm.numericalcalculus.result.AproximationResult;
import br.edu.ufabc.pdm.numericalcalculus.singleton.AproximationsDao;

/**
 * Created by eduardo on 26/04/15.
 */
public class AproximationService extends IntentService {

    public static final String BROADCAST_ACTION = AproximationService.class.getSimpleName() + ".DONE";
    private ArrayList<Double> iteractions;
    private AproximationsDao dao;

    public AproximationService() {
        super("AproximationService");
        iteractions = new ArrayList<Double>();
        dao = AproximationsDao.instantiate(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String method = intent.getExtras().getString("method");
        String function = intent.getExtras().getString("function");
        double initialInterval;
        double finalInterval;
        int repeat = intent.getExtras().getInt("repeat");
        int count = 1;
        double result = 0;
        String numbers = null;

        AproximationResult resultObj = new AproximationResult();
        resultObj.setMethod(method);
        resultObj.setFunction(function);
        resultObj.setRepeat(repeat);


        if (method.equals(Bisection.class.getSimpleName())) {
            initialInterval = intent.getExtras().getDouble("initialInterval");
            finalInterval = intent.getExtras().getDouble("finalInterval");
            result = bisection(function, initialInterval, finalInterval, repeat, count);
            numbers = "Intervalo de "+initialInterval+" à "+finalInterval;
            resultObj.setNumbers(numbers);

        } else if (method.equals(FixedPoint.class.getSimpleName())) {
            initialInterval = intent.getExtras().getDouble("initialNumber");
            result = fixedPoint(function, initialInterval, repeat, count);
            numbers = "Chute inicial: "+initialInterval;
            resultObj.setNumbers(numbers);

        } else if (method.equals(NewtonRaphson.class.getSimpleName())) {
            initialInterval = intent.getExtras().getDouble("initialNumber");
            result = newtonRaphson(function, initialInterval, repeat, count);
            numbers = "Chute inicial: "+initialInterval;
            resultObj.setNumbers(numbers);

        } else if (method.equals(Secant.class.getSimpleName())) {
            initialInterval = intent.getExtras().getDouble("firstNumber");
            finalInterval = intent.getExtras().getDouble("secondNumber");
            result = secant(function, initialInterval, finalInterval, repeat, count);
            numbers = "Pontos de amostra: "+initialInterval+" e "+finalInterval;
            resultObj.setNumbers(numbers);

        }

        Intent localIntent = new Intent(BROADCAST_ACTION);
        localIntent.putExtra("method", method);
        localIntent.putExtra("function", function);
        localIntent.putExtra("result", result);
        localIntent.putExtra("iteractions", iteractions);
        localIntent.putExtra("repeat", repeat);
        localIntent.putExtra("numbers", numbers);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

        resultObj.setResult(result);
        dao.add(resultObj);
        Log.w("AproximationService", "RESULT CALCULATED, ADDING TO DATABASE");
    }

    private Double bisection(String function, double initialInterval, double finalInterval, int repeat, int count) {

        Double mid = initialInterval + Math.abs((initialInterval - finalInterval)/2);

        Expression e = new ExpressionBuilder(function)
                .variables("x")
                .build();
        e.setVariable("x", initialInterval);
        double intI = e.evaluate();
        e.setVariable("x", finalInterval);
        double intF = e.evaluate();
        e.setVariable("x", mid);
        double intM = e.evaluate();
        Double result = null;

        iteractions.add(mid);
        if(count<=repeat) {
            count++;
            if ((intI <= 0) && (intM >= 0)) {
                result = bisection(function, initialInterval, mid, repeat, count);
            } else if ((intI >= 0) && (intM <= 0)) {
                result = bisection(function, initialInterval, mid, repeat, count);
            } else if ((intF <= 0) && (intM >= 0)) {
                result = bisection(function, finalInterval, mid, repeat, count);
            } else if  ((intF >= 0) && (intM <= 0)) {
                result = bisection(function, finalInterval, mid, repeat, count);
            }
        }
        else {
            result = mid;
        }
        return result;
    }

    private Double fixedPoint(String function, double initialNumber, int repeat, int count) {


        Expression e = new ExpressionBuilder(function)
                .variables("x")
                .build();

        e.setVariable("x", initialNumber);
        Double intI = e.evaluate();
        Double result = null;

        iteractions.add(initialNumber);
        if(count<=repeat) {
            count++;
            result = fixedPoint(function, intI, repeat, count);
        }
        else {
            result = initialNumber;
        }
        return result;
    }

    public Double newtonRaphson(String function, double initialNumber, int repeat, int count) {

        Double result = null;
        Expression e = new ExpressionBuilder(function)
                .variables("x")
                .build();

        e.setVariable("x", initialNumber);
        double fx = e.evaluate();

        e.setVariable("x", initialNumber + 0.000000001);
        double fdx = e.evaluate();
        double diff = (fdx - fx) / 0.000000001;
        double next = initialNumber - (fx / diff);

        iteractions.add(next);
        if (count<=repeat) {
            count++;
            result = newtonRaphson(function, next, repeat, count);
        } else {
            result = next;
        }
        return result;
    }

    private Double secant(String function, double firstNumber, double secondNumber, int repeat, int count) {
        Expression e = new ExpressionBuilder(function)
                .variables("x")
                .build();

        e.setVariable("x", firstNumber);
        Double intI = e.evaluate();

        e.setVariable("x", secondNumber);
        Double intF = e.evaluate();
        Double next = (firstNumber * intF - secondNumber * intI)/(intF-intI);
        Double result = null;

        iteractions.add(firstNumber);
        if((count<=repeat) && (firstNumber != secondNumber)) {
            count++;
            result = secant(function, secondNumber, next, repeat, count);
        }
        else {
            result = firstNumber;
        }
        return result;
    }
}
