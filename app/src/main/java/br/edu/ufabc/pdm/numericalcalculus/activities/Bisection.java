package br.edu.ufabc.pdm.numericalcalculus.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import br.edu.ufabc.pdm.numericalcalculus.Fragments.AproximationMethodFragment;
import br.edu.ufabc.pdm.numericalcalculus.Fragments.ResultFragment;
import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.services.AproximationService;

public class Bisection extends ActionBarActivity {

    private AproximationMethodFragment methodFragment;
    private ResultFragment resultFragment;
    private ResultReceiver receiver;
    private boolean tablet;
    private static final String LOGTAG = Bisection.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aproximation);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        bindHandlers();

        Configuration config = getResources().getConfiguration();
        tablet = config.isLayoutSizeAtLeast(Configuration.SCREENLAYOUT_SIZE_XLARGE);
        if (tablet) {

            FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
            transaction1.add(R.id.method_container, methodFragment);
            transaction1.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction1.commit();

            FragmentTransaction transaction2 = getFragmentManager().beginTransaction();
            transaction2.add(R.id.result_container, resultFragment);
            transaction2.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction2.commit();
        }
        else {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.method_container, methodFragment);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            //transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    private void init() {
        methodFragment = new AproximationMethodFragment();
        methodFragment.setMethod(Bisection.class.getSimpleName());
        resultFragment = new ResultFragment();

        IntentFilter calculationDone = new IntentFilter(AproximationService.BROADCAST_ACTION);
        receiver = new ResultReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, calculationDone);
    }

    private void bindHandlers() {
        final Context self = this;

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOGTAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOGTAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOGTAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOGTAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOGTAG, "onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(LOGTAG, "onRestart");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bisection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            String method = "bisection";
            Intent help = new Intent(this, About.class);
            help.putExtra("method", method);
            startActivity(help);
            return true;
        }
        if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void Send(View view) {



        final EditText functionInput = (EditText)findViewById(R.id.function);
        String function = functionInput.getText().toString();

        final EditText initialIntervalInput = (EditText)findViewById(R.id.initialInterval);
        String initialInterval = initialIntervalInput.getText().toString();

        final EditText finalIntervalInput = (EditText)findViewById(R.id.finalInterval);
        String finalInterval = finalIntervalInput.getText().toString();

        final EditText repeatInput = (EditText)findViewById(R.id.repeat);
        String repeatS = repeatInput.getText().toString();

        //String function = functionFull.replaceAll("([a-wyzA-WYZ,!@#$%&;\\\\|<>\\\"'?])", "");

        if(function == null || function.isEmpty()) {
            functionInput.requestFocus();
            functionInput.setSelected(true);
            functionInput.setError("Campo Obrigatório");
            return;
        }
        if(!function.contains("x")) {
            functionInput.requestFocus();
            functionInput.setSelected(true);
            functionInput.setError("Use X como variável");
            return;
        }
        if(initialInterval == null || initialInterval.isEmpty()) {
            initialIntervalInput.requestFocus();
            initialIntervalInput.setSelected(true);
            initialIntervalInput.setError("Campo Obrigatório");
            return;
        }
        if(finalInterval == null || finalInterval.isEmpty()) {
            finalIntervalInput.requestFocus();
            finalIntervalInput.setSelected(true);
            finalIntervalInput.setError("Campo Obrigatório");
            return;
        }
        if(repeatS == null || repeatS.isEmpty()) {
            repeatInput.requestFocus();
            repeatInput.setSelected(true);
            repeatInput.setError("Campo Obrigatório");
            return;
        }
    }

    private class ResultReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            FragmentManager fm = getFragmentManager();
            if (tablet && !isFinishing()) {
                fm.beginTransaction()
                        .remove(resultFragment)
                        .commit();

                resultFragment = new ResultFragment();
                resultFragment.setup(intent.getExtras().getDouble("result"), (ArrayList<Double>) intent.getExtras().getSerializable("iteractions"), intent.getExtras().getString("function"), intent.getExtras().getString("method"), intent.getExtras().getInt("repeat"), intent.getExtras().getString("numbers"));
                fm.beginTransaction()
                        .add(R.id.result_container, resultFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null)
                        .commit();
            } else if(!isFinishing()) {
                resultFragment.setup(intent.getExtras().getDouble("result"), (ArrayList<Double>) intent.getExtras().getSerializable("iteractions"), intent.getExtras().getString("function"), intent.getExtras().getString("method"), intent.getExtras().getInt("repeat"), intent.getExtras().getString("numbers"));
                fm.beginTransaction()
                        .replace(R.id.method_container, resultFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null)
                        .commit();
            }
        }
    }
}
