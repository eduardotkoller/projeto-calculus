package br.edu.ufabc.pdm.numericalcalculus.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import br.edu.ufabc.pdm.numericalcalculus.Fragments.ResultSystemFragment;
import br.edu.ufabc.pdm.numericalcalculus.Fragments.TransictionFragment;
import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.services.SystemService;

/**
 * Created by Felipe on 30/04/2015.
 */
public class Gauss extends ActionBarActivity {
    private TransictionFragment transictionFragment;
    private ResultSystemFragment resultSystemFragment;
    private boolean tablet;
    private ResultReceiver receiver;
    private static final String LOGTAG = FixedPoint.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matrizes);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        bindHandlers();

        Configuration config = getResources().getConfiguration();
        tablet = config.isLayoutSizeAtLeast(Configuration.SCREENLAYOUT_SIZE_XLARGE);
        if (tablet) {

            FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
            transaction1.add(R.id.method_container, transictionFragment);
            transaction1.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction1.commit();

            FragmentTransaction transaction2 = getFragmentManager().beginTransaction();
            transaction2.add(R.id.result_container, resultSystemFragment);
            transaction2.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction2.commit();
        }
        else {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.method_container, transictionFragment);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            //transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    private void init() {
        transictionFragment = new TransictionFragment();
        transictionFragment.setMethod(Gauss.class.getSimpleName());
        resultSystemFragment = new ResultSystemFragment();

        IntentFilter calculationDone = new IntentFilter(SystemService.BROADCAST_ACTION);
        receiver = new ResultReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, calculationDone);
    }

    private void bindHandlers() {
        final Context self = this;

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fixed_point, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            String method = "gauss";
            Intent help = new Intent(this, About.class);
            help.putExtra("method", method);
            startActivity(help);
            return true;
        }
        if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private class ResultReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            FragmentManager fm = getFragmentManager();
            if (tablet && !isFinishing()) {
                fm.beginTransaction()
                        .remove(resultSystemFragment)
                        .commit();

                resultSystemFragment = new ResultSystemFragment();
                resultSystemFragment.setup1(intent.getExtras().getLong("type"), (Double[])intent.getExtras().getSerializable("result"), intent.getExtras().getString("method"), intent.getExtras().getDouble("a11"), intent.getExtras().getDouble("a12"), intent.getExtras().getDouble("a13"), intent.getExtras().getDouble("a21"), intent.getExtras().getDouble("a22"), intent.getExtras().getDouble("a23"), intent.getExtras().getDouble("a31"), intent.getExtras().getDouble("a32"), intent.getExtras().getDouble("a33"), intent.getExtras().getDouble("b1"), intent.getExtras().getDouble("b2"), intent.getExtras().getDouble("b3"));

                fm.beginTransaction()
                        .add(R.id.result_container, resultSystemFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null)
                        .commit();
            } else if(!isFinishing()) {
                resultSystemFragment.setup1(intent.getExtras().getLong("type"), (Double[])intent.getExtras().getSerializable("result"), intent.getExtras().getString("method"), intent.getExtras().getDouble("a11"), intent.getExtras().getDouble("a12"), intent.getExtras().getDouble("a13"), intent.getExtras().getDouble("a21"), intent.getExtras().getDouble("a22"), intent.getExtras().getDouble("a23"), intent.getExtras().getDouble("a31"), intent.getExtras().getDouble("a32"), intent.getExtras().getDouble("a33"), intent.getExtras().getDouble("b1"), intent.getExtras().getDouble("b2"), intent.getExtras().getDouble("b3"));

                fm.beginTransaction()
                        .replace(R.id.method_container, resultSystemFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null)
                        .commit();
            }
        }
    }

}
