package br.edu.ufabc.pdm.numericalcalculus.activities;

import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.edu.ufabc.pdm.numericalcalculus.R;


public class About extends ActionBarActivity {

    private String method;
    private TextView title;
    private TextView description;

    public void setMethod(String method) {
        this.method = method;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

    }
    private void init() {
        method = getIntent().getStringExtra("method");
        title = (TextView) findViewById(R.id.titleAbout);
        description = (TextView) findViewById(R.id.description);

        if (method.equals("bisection")) {
            title.setText(R.string.title_activity_bisection);
            description.setText(R.string.bisection_help_text);
        } else if (method.equals("fixedPoint")) {
            title.setText(R.string.title_activity_fixed_point);
            description.setText(R.string.fixed_point_help_text);
        } else if (method.equals("newton-raphson")) {
            title.setText(R.string.title_activity_newton_raphson);
            description.setText(R.string.newton_raphson_help_text);
        }else if (method.equals("secant")) {
            title.setText(R.string.title_activity_secant);
            description.setText(R.string.secant_help_text);
        }else if (method.equals("gauss")) {
            title.setText(R.string.title_activity_gauss);
            description.setText(R.string.gauss_help_text);
        }else if (method.equals("midPoint")) {
            title.setText(R.string.title_activity_midPoint);
            description.setText(R.string.midpoint_help_text);
        }else if (method.equals("trapeze")) {
            title.setText(R.string.title_activity_trapeze);
            description.setText(R.string.trapzes_help_text);
        }else if (method.equals("simpson")) {
            title.setText(R.string.title_activity_simpson);
            description.setText(R.string.simpsons_help_text);
        }else if (method.equals("mainMenu")) {
            title.setText(R.string.title_about_version);
            description.setVisibility(View.GONE);
            findViewById(R.id.numerycal_logo).setVisibility(View.VISIBLE);
            ImageView logo = (ImageView)findViewById(R.id.numerycal_logo);
            logo.setImageResource(R.mipmap.ic_launcher);
            findViewById(R.id.developedby).setVisibility(View.VISIBLE);
            findViewById(R.id.eduardo).setVisibility(View.VISIBLE);
            findViewById(R.id.felipe).setVisibility(View.VISIBLE);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
