package br.edu.ufabc.pdm.numericalcalculus.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import br.edu.ufabc.pdm.numericalcalculus.activities.MidPoint;
import br.edu.ufabc.pdm.numericalcalculus.activities.Simpson;
import br.edu.ufabc.pdm.numericalcalculus.activities.Trapeze;
import br.edu.ufabc.pdm.numericalcalculus.result.IntegrationResult;
import br.edu.ufabc.pdm.numericalcalculus.singleton.IntegrationsDao;

/**
 * Created by eduardo on 28/04/15.
 */
public class IntegrationService extends IntentService {

    public static final String BROADCAST_ACTION = IntegrationService.class.getPackage() + ".DONE";
    private IntegrationsDao dao;

    public IntegrationService() {
        super("IntegrationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String method = intent.getExtras().getString("method");
        String function = intent.getExtras().getString("function");
        double initialInterval = intent.getExtras().getDouble("initialInterval");
        double finalInterval = intent.getExtras().getDouble("finalInterval");
        int intervals = intent.getExtras().getInt("intervals");
        double result = 0;
        dao = IntegrationsDao.instantiate(this);
        String numbers = "Intervalo de "+initialInterval+" à "+finalInterval;

        if (method.equals(MidPoint.class.getSimpleName())) {
            result = midpoint(function, initialInterval, finalInterval, intervals);
        }
        if (method.equals(Trapeze.class.getSimpleName())) {
           result = trapeze(function, initialInterval, finalInterval, intervals);
        }
        if (method.equals(Simpson.class.getSimpleName())) {
            result = simpson(function, initialInterval, finalInterval, intervals);
        }

        Intent localIntent = new Intent(BROADCAST_ACTION);
        localIntent.putExtra("function", function);
        localIntent.putExtra("initialInterval", initialInterval);
        localIntent.putExtra("finalInterval", finalInterval);
        localIntent.putExtra("method", method);
        localIntent.putExtra("intervals", intervals);
        localIntent.putExtra("result", result);
        localIntent.putExtra("numbers", numbers);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

        IntegrationResult resultObj = new IntegrationResult();
        resultObj.setMethod(method);
        resultObj.setFunction(function);
        resultObj.setNumbers(numbers);
        resultObj.setIntervals(intervals);
        resultObj.setResult(result);
        dao.add(resultObj);
    }

    private double midpoint(String function, double initialInterval, double finalInterval, int intervals) {
        Expression e = new ExpressionBuilder(function)
                .variables("x")
                .build();
        double h = Math.abs((finalInterval - initialInterval) / intervals);
        double result = 0;
        for (int i = 0; i < intervals; i++) {
            e.setVariable("x", ((initialInterval+h*i)+initialInterval+h*(i+1))/2);
            double fx = e.evaluate();
            result += h*fx;
        }
        return result;
    }
    private double trapeze(String function, double initialInterval, double finalInterval, int intervals) {
        Expression e = new ExpressionBuilder(function)
                .variables("x")
                .build();
        double h = Math.abs((finalInterval - initialInterval) / intervals);
        double result = 0;
        for (int i = 0; i < intervals; i++) {
            e.setVariable("x", initialInterval+h*i);
            double fx1 = e.evaluate();
            e.setVariable("x", initialInterval+h*(i+1));
            double fx2 = e.evaluate();
            result += h*(fx1+fx2)/2;
        }
        return result;
    }
    private double simpson(String function, double initialInterval, double finalInterval, int intervals) {
        Expression e = new ExpressionBuilder(function)
                .variables("x")
                .build();
        double h = Math.abs((finalInterval - initialInterval) / intervals);
        double result = 0;
        int c = 2;

        e.setVariable("x", initialInterval);
        double fx = e.evaluate();
        result+= h*fx/3;

        e.setVariable("x", initialInterval+h*(intervals-1));
        fx = e.evaluate();
        result+= h*fx/3;

        for (int i = 1; i < intervals-1; i++) {
            switch (c) {
                case 2:
                    c = 4;
                    break;
                case 4:
                    c = 2;
                    break;
            }
            e.setVariable("x", initialInterval + h*i);
            fx = e.evaluate();
            result += h*c*fx/3;
        }
        return result;
    }
}
