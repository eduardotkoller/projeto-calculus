package br.edu.ufabc.pdm.numericalcalculus.result;

import java.util.HashMap;

/**
 * Created by eduardo on 02/05/15.
 */
public class SystemResult {
    private HashMap<String, Double> coefs;
    private Double[] result;
    private long type;
    private long id;

    public HashMap<String, Double> getCoefs() {
        return coefs;
    }

    public void setCoefs(HashMap<String, Double> coefs) {
        this.coefs = coefs;
    }

    public Double[] getResult() {
        return result;
    }

    public void setResult(Double[] result) {
        this.result = result;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
