package br.edu.ufabc.pdm.numericalcalculus.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import br.edu.ufabc.pdm.numericalcalculus.activities.Gauss;
import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.services.SystemService;

/**
 * Created by Felipe on 30/04/2015.
 */
public class SystemFragment extends Fragment {

    private String method;
    private long type;
    public static long M2 = 1;
    public static long M3 = 2;

    public void setMethod(String method) {
        this.method = method;
    }

    public void setSize(long type) {
        this.type = type;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_matrizes2x2_method, container, false);
        if (type == M2) view = inflater.inflate(R.layout.fragment_matrizes2x2_method, container, false);
        else if (type == M3) view = inflater.inflate(R.layout.fragment_matrizes3x3, container, false);
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        final EditText a11 = (EditText) view.findViewById(R.id.a11);
        final EditText a12 = (EditText) view.findViewById(R.id.a12);
        final EditText a21 = (EditText) view.findViewById(R.id.a21);
        final EditText a22 = (EditText) view.findViewById(R.id.a22);
        final EditText b1 = (EditText) view.findViewById(R.id.b1);
        final EditText b2 = (EditText) view.findViewById(R.id.b2);
        final EditText a13 = (EditText) view.findViewById(R.id.a13);
        final EditText a23 = (EditText) view.findViewById(R.id.a23);
        final EditText a31 = (EditText) view.findViewById(R.id.a31);
        final EditText a32 = (EditText) view.findViewById(R.id.a32);
        final EditText a33 = (EditText) view.findViewById(R.id.a33);
        final EditText b3 = (EditText) view.findViewById(R.id.b3);

        Button button = (Button) view.findViewById(R.id.button_calculate);
        button.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Intent service = new Intent(v.getContext(), SystemService.class);
                if (method.equals(Gauss.class.getSimpleName())) {
                    if (type==SystemFragment.M2) {

                        if(a11.getText().toString().isEmpty()) {
                            a11.requestFocus();
                            a11.setSelected(true);
                            a11.setError("Campo Obrigatório");
                            return;
                        }

                        if(a12.getText().toString().isEmpty()) {
                            a12.requestFocus();
                            a12.setSelected(true);
                            a12.setError("Campo Obrigatório");
                            return;
                        }

                        if(a21.getText().toString().isEmpty()) {
                            a21.requestFocus();
                            a21.setSelected(true);
                            a21.setError("Campo Obrigatório");
                            return;
                        }

                        if(a22.getText().toString().isEmpty()) {
                            a22.requestFocus();
                            a22.setSelected(true);
                            a22.setError("Campo Obrigatório");
                            return;
                        }

                        if(b1.getText().toString().isEmpty()) {
                            b1.requestFocus();
                            b1.setSelected(true);
                            b1.setError("Campo Obrigatório");
                            return;
                        }

                        if(b2.getText().toString().isEmpty()) {
                            b2.requestFocus();
                            b2.setSelected(true);
                            b2.setError("Campo Obrigatório");
                            return;
                        }
                        service.putExtra("a11", Double.parseDouble(a11.getText().toString()));
                        service.putExtra("a12", Double.parseDouble(a12.getText().toString()));
                        service.putExtra("a21", Double.parseDouble(a21.getText().toString()));
                        service.putExtra("a22", Double.parseDouble(a22.getText().toString()));
                        service.putExtra("b1", Double.parseDouble(b1.getText().toString()));
                        service.putExtra("b2", Double.parseDouble(b2.getText().toString()));
                    } else if(type==SystemFragment.M3) {

                        if(a11.getText().toString().isEmpty()) {
                            a11.requestFocus();
                            a11.setSelected(true);
                            a11.setError("Campo Obrigatório");
                            return;
                        }

                        if(a12.getText().toString().isEmpty()) {
                            a12.requestFocus();
                            a12.setSelected(true);
                            a12.setError("Campo Obrigatório");
                            return;
                        }

                        if(a13.getText().toString().isEmpty()) {
                            a13.requestFocus();
                            a13.setSelected(true);
                            a13.setError("Campo Obrigatório");
                            return;
                        }

                        if(a21.getText().toString().isEmpty()) {
                            a21.requestFocus();
                            a21.setSelected(true);
                            a21.setError("Campo Obrigatório");
                            return;
                        }

                        if(a22.getText().toString().isEmpty()) {
                            a22.requestFocus();
                            a22.setSelected(true);
                            a22.setError("Campo Obrigatório");
                            return;
                        }

                        if(a23.getText().toString().isEmpty()) {
                            a23.requestFocus();
                            a23.setSelected(true);
                            a23.setError("Campo Obrigatório");
                            return;
                        }

                        if(a31.getText().toString().isEmpty()) {
                            a31.requestFocus();
                            a31.setSelected(true);
                            a31.setError("Campo Obrigatório");
                            return;
                        }

                        if(a32.getText().toString().isEmpty()) {
                            a32.requestFocus();
                            a32.setSelected(true);
                            a32.setError("Campo Obrigatório");
                            return;
                        }

                        if(a33.getText().toString().isEmpty()) {
                            a33.requestFocus();
                            a33.setSelected(true);
                            a33.setError("Campo Obrigatório");
                            return;
                        }

                        if(b1.getText().toString().isEmpty()) {
                            b1.requestFocus();
                            b1.setSelected(true);
                            b1.setError("Campo Obrigatório");
                            return;
                        }

                        if(b2.getText().toString().isEmpty()) {
                            b2.requestFocus();
                            b2.setSelected(true);
                            b2.setError("Campo Obrigatório");
                            return;
                        }

                        if(b3.getText().toString().isEmpty()) {
                            b3.requestFocus();
                            b3.setSelected(true);
                            b3.setError("Campo Obrigatório");
                            return;
                        }

                        service.putExtra("a11", Double.parseDouble(a11.getText().toString()));
                        service.putExtra("a12", Double.parseDouble(a12.getText().toString()));
                        service.putExtra("a13", Double.parseDouble(a13.getText().toString()));
                        service.putExtra("a21", Double.parseDouble(a21.getText().toString()));
                        service.putExtra("a22", Double.parseDouble(a22.getText().toString()));
                        service.putExtra("a23", Double.parseDouble(a23.getText().toString()));
                        service.putExtra("a31", Double.parseDouble(a31.getText().toString()));
                        service.putExtra("a32", Double.parseDouble(a32.getText().toString()));
                        service.putExtra("a33", Double.parseDouble(a33.getText().toString()));
                        service.putExtra("b1", Double.parseDouble(b1.getText().toString()));
                        service.putExtra("b2", Double.parseDouble(b2.getText().toString()));
                        service.putExtra("b3", Double.parseDouble(b3.getText().toString()));
                    }
                    service.putExtra("method", method);
                    service.putExtra("type", type);
                }
                v.getContext().startService(service);
            }
        });
    }
}