package br.edu.ufabc.pdm.numericalcalculus.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.edu.ufabc.pdm.numericalcalculus.R;

/**
 * Created by Felipe on 01/05/2015.
 */
public class ResultSystemFragment extends Fragment {

    private static final String LOGTAG = ResultSystemFragment.class.getSimpleName();

    private Double[] result;
    private Double a11;
    private Double a12;
    private Double a13;
    private Double a21;
    private Double a22;
    private Double a23;
    private Double a31;
    private Double a32;
    private Double a33;
    private Double b1;
    private Double b2;
    private Double b3;
    private long type;
    public View view;
    private String method;

    public void setup1(long type, Double[] result, String method, Double a11, Double a12, Double a13, Double a21, Double a22, Double a23, Double a31, Double a32, Double a33, Double b1, Double b2, Double b3) {

        this.result = result;
        this.method = method;
        this.a11 = a11;
        this.a12 = a12;
        this.a13 = a13;
        this.a21 = a21;
        this.a22 = a22;
        this.a23 = a23;
        this.a31 = a31;
        this.a32 = a32;
        this.a33 = a33;
        this.b1 = b1;
        this.b2 = b2;
        this.b3 = b3;
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.result_system_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        TextView a11 = (TextView) view.findViewById(R.id.a11);
        TextView a12 = (TextView) view.findViewById(R.id.a12);
        TextView a13 = (TextView) view.findViewById(R.id.a13);
        TextView a21 = (TextView) view.findViewById(R.id.a21);
        TextView a22 = (TextView) view.findViewById(R.id.a22);
        TextView a23 = (TextView) view.findViewById(R.id.a23);
        TextView a31 = (TextView) view.findViewById(R.id.a31);
        TextView a32 = (TextView) view.findViewById(R.id.a32);
        TextView a33 = (TextView) view.findViewById(R.id.a33);
        TextView b1 = (TextView) view.findViewById(R.id.b1);
        TextView b2 = (TextView) view.findViewById(R.id.b2);
        TextView b3 = (TextView) view.findViewById(R.id.b3);
        TextView method = (TextView) view.findViewById(R.id.result_method);
        TextView x1 = (TextView) view.findViewById(R.id.result_numbers1);
        TextView x2 = (TextView) view.findViewById(R.id.result_numbers2);
        TextView x3 = (TextView) view.findViewById(R.id.result_numbers3);
        if(type== SystemFragment.M2) {
            x1.setText("x = "+String.valueOf(this.result[0]));
            x2.setText("y = "+String.valueOf(this.result[1]));
            a11.setText(String.valueOf(this.a11));
            a12.setText(String.valueOf(this.a12));
            a21.setText(String.valueOf(this.a21));
            a22.setText(String.valueOf(this.a22));
            b1.setText(String.valueOf(this.b1));
            b2.setText(String.valueOf(this.b2));
            a13.setVisibility(View.GONE);
            a23.setVisibility(View.GONE);
            a31.setVisibility(View.GONE);
            a32.setVisibility(View.GONE);
            a33.setVisibility(View.GONE);
            b3.setVisibility(View.GONE);
        } else if(type==SystemFragment.M3) {
            x1.setText("x = "+String.valueOf(this.result[0]));
            x2.setText("y = "+String.valueOf(this.result[1]));
            x3.setText("z = "+String.valueOf(this.result[2]));
            a11.setText(String.valueOf(this.a11));
            a12.setText(String.valueOf(this.a12));
            a13.setText(String.valueOf(this.a13));
            a21.setText(String.valueOf(this.a21));
            a22.setText(String.valueOf(this.a22));
            a23.setText(String.valueOf(this.a23));
            a31.setText(String.valueOf(this.a31));
            a32.setText(String.valueOf(this.a32));
            a33.setText(String.valueOf(this.a33));
            b1.setText(String.valueOf(this.b1));
            b2.setText(String.valueOf(this.b2));
            b3.setText(String.valueOf(this.b3));
        }
        method.setText(this.method);
    }
}