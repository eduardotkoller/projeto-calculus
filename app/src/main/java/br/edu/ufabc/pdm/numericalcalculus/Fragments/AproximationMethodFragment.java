package br.edu.ufabc.pdm.numericalcalculus.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import br.edu.ufabc.pdm.numericalcalculus.activities.Bisection;
import br.edu.ufabc.pdm.numericalcalculus.activities.FixedPoint;
import br.edu.ufabc.pdm.numericalcalculus.activities.NewtonRaphson;
import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.activities.Secant;
import br.edu.ufabc.pdm.numericalcalculus.services.AproximationService;

/**
 * Created by Felipe on 29/04/15.
 */ 
public class AproximationMethodFragment extends Fragment {

    private static final String LOGTAG = AproximationMethodFragment.class.getSimpleName();
    private String method;

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_aproximation_method, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        final EditText function = (EditText )view.findViewById(R.id.function);
        final EditText initialInterval = (EditText )view.findViewById(R.id.initialInterval);
        final EditText finalInterval = (EditText )view.findViewById(R.id.finalInterval);
        final EditText initialNumber = (EditText )view.findViewById(R.id.initialNumber);
        final EditText firstNumber = (EditText )view.findViewById(R.id.firstNumber);
        final EditText secondNumber = (EditText )view.findViewById(R.id.secondNumber);
        final EditText repeat = (EditText )view.findViewById(R.id.repeat);

        if (method.equals(FixedPoint.class.getSimpleName())) {
            function.setHint("Digite a funcão G(x)");
            initialInterval.setVisibility(View.GONE);
            finalInterval.setVisibility(View.GONE);
            firstNumber.setVisibility(View.GONE);
            secondNumber.setVisibility(View.GONE);
        } else if(method.equals(Bisection.class.getSimpleName())) {
            firstNumber.setVisibility(View.GONE);
            secondNumber.setVisibility(View.GONE);
            initialNumber.setVisibility(View.GONE);
        } else if(method.equals(NewtonRaphson.class.getSimpleName())) {
            initialInterval.setVisibility(View.GONE);
            finalInterval.setVisibility(View.GONE);
            firstNumber.setVisibility(View.GONE);
            secondNumber.setVisibility(View.GONE);
        } else if(method.equals(Secant.class.getSimpleName())) {
            initialInterval.setVisibility(View.GONE);
            finalInterval.setVisibility(View.GONE);
            initialNumber.setVisibility(View.GONE);
        }

        Button button = (Button )view.findViewById(R.id.button_calculate);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent service = new Intent(v.getContext(), AproximationService.class);
                if (method.equals(FixedPoint.class.getSimpleName())) {
                    if(function.getText().toString().isEmpty()) {
                        function.requestFocus();
                        function.setSelected(true);
                        function.setError("Campo Obrigatório");
                        return;
                    }
                    if(!function.getText().toString().contains("x")) {
                        function.requestFocus();
                        function.setSelected(true);
                        function.setError("Use x como variável");
                        return;
                    }
                    if(initialNumber.getText().toString().isEmpty()) {
                        initialNumber.requestFocus();
                        initialNumber.setSelected(true);
                        initialNumber.setError("Campo Obrigatório");
                        return;
                    }
                    if(repeat.getText().toString().isEmpty()) {
                        repeat.requestFocus();
                        repeat.setSelected(true);
                        repeat.setError("Campo Obrigatório");
                        return;
                    }
                    service.putExtra("initialNumber", Double.parseDouble(initialNumber.getText().toString()));
                } else if(method.equals(Bisection.class.getSimpleName())) {
                    if(function.getText().toString().isEmpty()) {
                        function.requestFocus();
                        function.setSelected(true);
                        function.setError("Campo Obrigatório");
                        return;
                    }
                    if(!function.getText().toString().contains("x")) {
                        function.requestFocus();
                        function.setSelected(true);
                        function.setError("Use x como variável");
                        return;
                    }
                    if(repeat.getText().toString().isEmpty()) {
                        repeat.requestFocus();
                        repeat.setSelected(true);
                        repeat.setError("Campo Obrigatório");
                        return;
                    }
                    if(initialInterval.getText().toString().isEmpty()) {
                        initialInterval.requestFocus();
                        initialInterval.setSelected(true);
                        initialInterval.setError("Campo Obrigatório");
                        return;
                    }
                    if(finalInterval.getText().toString().isEmpty()) {
                        finalInterval.requestFocus();
                        finalInterval.setSelected(true);
                        finalInterval.setError("Campo Obrigatório");
                        return;
                    }
                    service.putExtra("initialInterval", Double.parseDouble(initialInterval.getText().toString()));
                    service.putExtra("finalInterval", Double.parseDouble(finalInterval.getText().toString()));
                } else if(method.equals(NewtonRaphson.class.getSimpleName())) {
                    if(function.getText().toString().isEmpty()) {
                        function.requestFocus();
                        function.setSelected(true);
                        function.setError("Campo Obrigatório");
                        return;
                    }
                    if(!function.getText().toString().contains("x")) {
                        function.requestFocus();
                        function.setSelected(true);
                        function.setError("Use x como variável");
                        return;
                    }
                    if(initialNumber.getText().toString().isEmpty()) {
                        initialNumber.requestFocus();
                        initialNumber.setSelected(true);
                        initialNumber.setError("Campo Obrigatório");
                        return;
                    }
                    if(repeat.getText().toString().isEmpty()) {
                        repeat.requestFocus();
                        repeat.setSelected(true);
                        repeat.setError("Campo Obrigatório");
                        return;
                    }
                    service.putExtra("initialNumber", Double.parseDouble(initialNumber.getText().toString()));
                } else if(method.equals(Secant.class.getSimpleName())) {
                    if(function.getText().toString().isEmpty()) {
                        function.requestFocus();
                        function.setSelected(true);
                        function.setError("Campo Obrigatório");
                        return;
                    }
                    if(!function.getText().toString().contains("x")) {
                        function.requestFocus();
                        function.setSelected(true);
                        function.setError("Use x como variável");
                        return;
                    }
                    if(repeat.getText().toString().isEmpty()) {
                        repeat.requestFocus();
                        repeat.setSelected(true);
                        repeat.setError("Campo Obrigatório");
                        return;
                    }
                    if(firstNumber.getText().toString().isEmpty()) {
                        firstNumber.requestFocus();
                        firstNumber.setSelected(true);
                        firstNumber.setError("Campo Obrigatório");
                        return;
                    }
                    if(secondNumber.getText().toString().isEmpty()) {
                        secondNumber.requestFocus();
                        secondNumber.setSelected(true);
                        secondNumber.setError("Campo Obrigatório");
                        return;
                    }
                    service.putExtra("firstNumber", Double.parseDouble(firstNumber.getText().toString()));
                    service.putExtra("secondNumber", Double.parseDouble(secondNumber.getText().toString()));
                }
                service.putExtra("method", method);
                service.putExtra("function", function.getText().toString());
                service.putExtra("repeat", Integer.parseInt(repeat.getText().toString()));
                v.getContext().startService(service);
            }
        });
    }
}
