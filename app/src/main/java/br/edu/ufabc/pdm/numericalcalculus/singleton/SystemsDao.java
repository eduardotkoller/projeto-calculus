package br.edu.ufabc.pdm.numericalcalculus.singleton;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.Fragments.SystemFragment;
import br.edu.ufabc.pdm.numericalcalculus.result.SystemResult;

/**
 * Created by eduardo on 30/04/15.
 */
public class SystemsDao extends SQLiteOpenHelper {
    private static SystemsDao dao;
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "systems.db";
    private Context context;
    private SQLiteDatabase db;
    private String LOGTAG = SystemsDao.class.getSimpleName();

    protected SystemsDao(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        db = getWritableDatabase();
    }

    public static SystemsDao instantiate(Context context) {
        if (dao == null) {
            dao = new SystemsDao(context);
        } else {
            dao.context = context;
        }
        return dao;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String queryStr = context.getResources().getString(R.string.db_matrices_create);

        try {
            db.execSQL(queryStr);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String queryStr = context.getString(R.string.drop_matrices_query);

        try {
            db.execSQL(queryStr);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to drop the database", e);
        }
    }

    public boolean add(SystemResult resultObject) {
        String queryStr = context.getString(R.string.insert_matrix_query);
        boolean status = true;

        long type = resultObject.getType();
        HashMap<String, Double> coefs = resultObject.getCoefs();
        Double[] result = resultObject.getResult();

        try {
            SQLiteStatement statement = db.compileStatement(queryStr);

            statement.bindLong(1, type);
            statement.bindDouble(2, coefs.get("a11"));
            statement.bindDouble(3, coefs.get("a12"));
            if  (type == SystemFragment.M3) statement.bindDouble(4, coefs.get("a13"));
            statement.bindDouble(5, coefs.get("a21"));
            statement.bindDouble(6, coefs.get("a22"));
            if  (type == SystemFragment.M3) statement.bindDouble(7, coefs.get("a23"));
            if  (type == SystemFragment.M3) statement.bindDouble(8, coefs.get("a31"));
            if  (type == SystemFragment.M3) statement.bindDouble(9, coefs.get("a32"));
            if  (type == SystemFragment.M3) statement.bindDouble(10, coefs.get("a33"));
            statement.bindDouble(11, coefs.get("b1"));
            statement.bindDouble(12, coefs.get("b2"));
            if  (type == SystemFragment.M3) statement.bindDouble(13, coefs.get("b3"));
            statement.bindDouble(14, result[0]);
            statement.bindDouble(15, result[1]);
            if  (type == SystemFragment.M3) statement.bindDouble(16, result[2]);
            statement.execute();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to add matrix to database", e);
            status = false;
        }

        return status;
    }
    public SystemResult getResultById(long id) {
        String query = context.getResources().getString(R.string.get_matrix_query, id);

        SystemResult result = new SystemResult();

        try {
            Cursor cursor = db.rawQuery(query, new String[]{});

            cursor.moveToFirst();
            HashMap<String, Double> coefs = new HashMap<String, Double>();
            result.setType(cursor.getLong(1));
            coefs.put("a11", cursor.getDouble(2));
            coefs.put("a12", cursor.getDouble(3));
            coefs.put("a13", cursor.getDouble(4));
            coefs.put("a21", cursor.getDouble(5));
            coefs.put("a22", cursor.getDouble(6));
            coefs.put("a23", cursor.getDouble(7));
            coefs.put("a31", cursor.getDouble(8));
            coefs.put("a32", cursor.getDouble(9));
            coefs.put("a33", cursor.getDouble(10));
            coefs.put("b1", cursor.getDouble(11));
            coefs.put("b2", cursor.getDouble(12));
            coefs.put("b3", cursor.getDouble(13));
            Double[] results = new Double[3];
            results[0] = cursor.getDouble(14);
            results[1] = cursor.getDouble(15);
            results[2] = cursor.getDouble(16);
            result.setResult(results);
            result.setCoefs(coefs);
            result.setId(cursor.getLong(0));
            cursor.close();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to get matrix from the database", e);
        }
        return result;
    }
    public ArrayList<SystemResult> list() {
        ArrayList<SystemResult> results = new ArrayList<>();
        String queryStr = context.getString(R.string.list_matrices_query);

        try {
            Cursor cursor = db.rawQuery(queryStr, new String[]{});

            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                SystemResult result = new SystemResult();


                HashMap<String, Double> coefs = new HashMap<String, Double>();
                result.setType(cursor.getLong(1));
                coefs.put("a11", cursor.getDouble(2));
                coefs.put("a12", cursor.getDouble(3));
                coefs.put("a13", cursor.getDouble(4));
                coefs.put("a21", cursor.getDouble(5));
                coefs.put("a22", cursor.getDouble(6));
                coefs.put("a23", cursor.getDouble(7));
                coefs.put("a31", cursor.getDouble(8));
                coefs.put("a32", cursor.getDouble(9));
                coefs.put("a33", cursor.getDouble(10));
                coefs.put("b1", cursor.getDouble(11));
                coefs.put("b2", cursor.getDouble(12));
                coefs.put("b3", cursor.getDouble(13));
                Double[] resultArray = new Double[3];
                resultArray[0] = cursor.getDouble(14);
                resultArray[1] = cursor.getDouble(15);
                resultArray[2] = cursor.getDouble(16);
                result.setResult(resultArray);
                result.setCoefs(coefs);
                result.setId(cursor.getLong(0));
                results.add(result);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to list matrices from the database", e);
        }

        return results;
    }
    public boolean clear() {
        boolean status = true;
        String queryStr = context.getString(R.string.delete_matrices_query);

        try {
            db.execSQL(queryStr);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to delete all entries from matrices databse", e);
            status = false;
        }
        return status;
    }
}
