package br.edu.ufabc.pdm.numericalcalculus.singleton;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;

import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.result.IntegrationResult;

/**
 * Created by eduardo on 30/04/15.
 */
public class IntegrationsDao extends SQLiteOpenHelper {
    private static IntegrationsDao dao;
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "integrations.db";
    private Context context;
    private SQLiteDatabase db;
    private String LOGTAG = IntegrationsDao.class.getSimpleName();

    protected IntegrationsDao(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        db = getWritableDatabase();
    }

    public static IntegrationsDao instantiate(Context context) {
        if (dao == null) {
            dao = new IntegrationsDao(context);
        } else {
            dao.context = context;
        }
        return dao;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String queryStr = context.getResources().getString(R.string.db_integrations_create);

        try {
            db.execSQL(queryStr);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String queryStr = context.getString(R.string.drop_integrations_query);

        try {
            db.execSQL(queryStr);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to drop the databse", e);
        }
    }

    public boolean add(IntegrationResult resultObject) {
        String queryStr = context.getString(R.string.insert_integration_query);
        boolean status = true;
        String method = resultObject.getMethod();
        String function = resultObject.getFunction();
        String numbers = resultObject.getNumbers();
        Integer intervals = resultObject.getIntervals();
        Double result = resultObject.getResult();


        try {
            SQLiteStatement statement = db.compileStatement(queryStr);

            statement.bindString(1, method) ;
            statement.bindString(2, function);
            statement.bindString(3, numbers);
            statement.bindDouble(4, intervals);
            statement.bindDouble(5, result);
            statement.execute();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to add aproximation to database", e);
            status = false;
        }

        return status;
    }
    public IntegrationResult getResultById(long id) {
        String query = context.getResources().getString(R.string.get_integration_query, id);

        IntegrationResult result = new IntegrationResult();

        try {
            Cursor cursor = db.rawQuery(query, new String[]{});

            cursor.moveToFirst();

            result.setId(cursor.getLong(0));
            result.setMethod(cursor.getString(1));
            result.setFunction(cursor.getString(2));
            result.setNumbers(cursor.getString(3));
            result.setIntervals(cursor.getInt(4));
            result.setResult(cursor.getDouble(5));
            cursor.close();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to list tasks from the database", e);
        }
        return result;
    }
    public ArrayList<IntegrationResult> list() {
        ArrayList<IntegrationResult> results = new ArrayList<>();
        String queryStr = context.getString(R.string.list_integrations_query);

        try {
            Cursor cursor = db.rawQuery(queryStr, new String[]{});

            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                IntegrationResult result = new IntegrationResult();

                result.setId(cursor.getLong(0));
                result.setMethod(cursor.getString(1));
                result.setFunction(cursor.getString(2));
                result.setNumbers(cursor.getString(3));
                result.setIntervals(cursor.getInt(4));
                result.setResult(cursor.getDouble(5));
                results.add(result);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to list integrations from the database", e);
        }

        return results;
    }
    public boolean clear() {
        boolean status = true;
        String queryStr = context.getString(R.string.delete_integrations_query);

        try {
            db.execSQL(queryStr);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to delete all entries from integrations databse", e);
            status = false;
        }
        return status;
    }
}
