package br.edu.ufabc.pdm.numericalcalculus.Fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;

import br.edu.ufabc.pdm.numericalcalculus.activities.Gauss;
import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.result.AproximationAdapter;
import br.edu.ufabc.pdm.numericalcalculus.result.AproximationResult;
import br.edu.ufabc.pdm.numericalcalculus.result.IntegrationAdapter;
import br.edu.ufabc.pdm.numericalcalculus.result.IntegrationResult;
import br.edu.ufabc.pdm.numericalcalculus.result.SystemAdapter;
import br.edu.ufabc.pdm.numericalcalculus.result.SystemResult;

/**
 * Created by eduardo on 30/04/15.
 */
public class HistoryFragment extends Fragment {

    private int type = -1;
    public static int APROXIMATION = 1;
    public static int INTEGRATION = 2;
    public static int MATRIX = 3;
    private ListView listview;

    public void setup (int type) {
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history_function, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final HistoryFragment self = this;
        listview = (ListView)view.findViewById(R.id.history);
        if (type == APROXIMATION) {
            AproximationAdapter adapter = new AproximationAdapter(view.getContext());
            listview.setAdapter(adapter);
        } else if(type == INTEGRATION) {
            IntegrationAdapter adapter = new IntegrationAdapter(view.getContext());
            listview.setAdapter(adapter);
        } else if(type == MATRIX) {
            SystemAdapter adapter = new SystemAdapter(view.getContext());
            listview.setAdapter(adapter);
        }
        else {
            String[] types = {"Aproximacões", "Integracões", "Sistemas"};
            ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_list_item_1, types);

            listview.setAdapter(typeAdapter);
        }
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listview.getAdapter().getItem(position).toString().equals("Aproximacões")) {
                    HistoryFragment newFragment = new HistoryFragment();
                    newFragment.setup(APROXIMATION);
                    getFragmentManager().beginTransaction().remove(self).add(R.id.history_container, newFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
                } else if (listview.getAdapter().getItem(position).toString().equals("Integracões")) {
                    HistoryFragment newFragment = new HistoryFragment();
                    newFragment.setup(INTEGRATION);
                    getFragmentManager().beginTransaction().remove(self).add(R.id.history_container, newFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
                } else if (listview.getAdapter().getItem(position).toString().equals("Sistemas")) {
                    HistoryFragment newFragment = new HistoryFragment();
                    newFragment.setup(MATRIX);
                    getFragmentManager().beginTransaction().remove(self).add(R.id.history_container, newFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
                } else {
                    if (listview.getAdapter().getItem(position) instanceof AproximationResult) {
                        AproximationResult resultObj = (AproximationResult)listview.getAdapter().getItem(position);
                        ResultFragment resultFragment = new ResultFragment();
                        resultFragment.setup(resultObj.getResult(), null, resultObj.getFunction(), resultObj.getMethod(), resultObj.getRepeat(), resultObj.getNumbers());
                        getFragmentManager().beginTransaction().remove(self).add(R.id.history_container, resultFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
                    } else if (listview.getAdapter().getItem(position) instanceof IntegrationResult) {
                        IntegrationResult resultObj = (IntegrationResult)listview.getAdapter().getItem(position);
                        ResultFragment resultFragment = new ResultFragment();
                        resultFragment.setup(resultObj.getResult(), null, resultObj.getFunction(), resultObj.getMethod(), resultObj.getIntervals(), resultObj.getNumbers());
                        getFragmentManager().beginTransaction().remove(self).add(R.id.history_container, resultFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
                    } else if (listview.getAdapter().getItem(position) instanceof SystemResult) {
                        SystemResult resultObj = (SystemResult)listview.getAdapter().getItem(position);
                        ResultSystemFragment resultFragment = new ResultSystemFragment();
                        HashMap<String, Double> coefs = resultObj.getCoefs();
                        resultFragment.setup1(resultObj.getType(), resultObj.getResult(), Gauss.class.getSimpleName(), coefs.get("a11"), coefs.get("a12"), coefs.get("a13"), coefs.get("a21"), coefs.get("a22"), coefs.get("a23"), coefs.get("a31"), coefs.get("a32"), coefs.get("a33"), coefs.get("b1"), coefs.get("b2"), coefs.get("b3"));
                        getFragmentManager().beginTransaction().remove(self).add(R.id.history_container, resultFragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
                    }
                }
            }
        });
    }
}
