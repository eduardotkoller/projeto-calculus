package br.edu.ufabc.pdm.numericalcalculus.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.services.IntegrationService;

/**
 * Created by Felipe on 29/04/15.
 */ 
public class IntegrationMethodFragment extends Fragment {

    private static final String LOGTAG = IntegrationMethodFragment.class.getSimpleName();
    private String method;

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_integration_method, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        final EditText function = (EditText )view.findViewById(R.id.function);
        final EditText initialInterval = (EditText )view.findViewById(R.id.initialInterval);
        final EditText finalInterval = (EditText )view.findViewById(R.id.finalInterval);
        final EditText intervals = (EditText )view.findViewById(R.id.intervals);

        Button button = (Button )view.findViewById(R.id.button_calculate);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(function.getText().toString().isEmpty()) {
                    function.requestFocus();
                    function.setSelected(true);
                    function.setError("Campo Obrigatório");
                    return;
                }
                if(!function.getText().toString().contains("x")) {
                    function.requestFocus();
                    function.setSelected(true);
                    function.setError("Use x como variável");
                    return;
                }
                if(initialInterval.getText().toString().isEmpty()) {
                    initialInterval.requestFocus();
                    initialInterval.setSelected(true);
                    initialInterval.setError("Campo Obrigatório");
                    return;
                }
                if(finalInterval.getText().toString().isEmpty()) {
                    finalInterval.requestFocus();
                    finalInterval.setSelected(true);
                    finalInterval.setError("Campo Obrigatório");
                    return;
                }
                if(intervals.getText().toString().isEmpty()) {
                    intervals.requestFocus();
                    intervals.setSelected(true);
                    intervals.setError("Campo Obrigatório");
                    return;
                }
                Intent service = new Intent(v.getContext(), IntegrationService.class);
                service.putExtra("function", function.getText().toString());
                service.putExtra("initialInterval", Double.parseDouble(initialInterval.getText().toString()));
                service.putExtra("finalInterval", Double.parseDouble(finalInterval.getText().toString()));
                service.putExtra("intervals", Integer.parseInt(intervals.getText().toString()));
                service.putExtra("method", method);
                v.getContext().startService(service);
            }
        });
    }

}
