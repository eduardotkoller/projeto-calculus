package br.edu.ufabc.pdm.numericalcalculus.singleton;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;

import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.result.AproximationResult;

/**
 * Created by eduardo on 30/04/15.
 */
public class AproximationsDao extends SQLiteOpenHelper {
    private static AproximationsDao dao;
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "aproximations.db";
    private Context context;
    private SQLiteDatabase db;
    private String LOGTAG = AproximationsDao.class.getSimpleName();

    protected AproximationsDao(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        db = getWritableDatabase();
    }

    public static AproximationsDao instantiate(Context context) {
        if (dao == null) {
            dao = new AproximationsDao(context);
        } else {
            dao.context = context;
        }
        return dao;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String queryStr = context.getResources().getString(R.string.db_aproximations_create);

        try {
            db.execSQL(queryStr);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String queryStr = context.getString(R.string.drop_aproximations_query);

        try {
            db.execSQL(queryStr);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to drop the databse", e);
        }
    }

    public int size() {
        String queryStr = context.getString(R.string.count_aproximations_query);
        int count = -1;

        try {
            Cursor cursor = db.rawQuery(queryStr, new String[]{});

            cursor.moveToFirst();
            count = cursor.getInt(0);
            cursor.close();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to count tasks in the database", e);
        }

        return count;
    }

    public boolean add(AproximationResult resultObject) {
        String queryStr = context.getString(R.string.insert_aproximation_query);
        boolean status = true;
        String method = resultObject.getMethod();
        String function = resultObject.getFunction();
        String numbers = resultObject.getNumbers();
        Integer repeat = resultObject.getRepeat();
        Double result = resultObject.getResult();


        try {
            SQLiteStatement statement = db.compileStatement(queryStr);

            statement.bindString(1, method) ;
            statement.bindString(2, function);
            statement.bindString(3, numbers);
            statement.bindDouble(4, repeat);
            statement.bindDouble(5, result);
            statement.execute();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to add aproximation to database", e);
            status = false;
        }

        return status;
    }
    public AproximationResult getResultById(long id) {
        String query = context.getResources().getString(R.string.get_aproximation_query, id);

        AproximationResult result = new AproximationResult();

        try {
            Cursor cursor = db.rawQuery(query, new String[]{});

            cursor.moveToFirst();

            result.setId(cursor.getLong(0));
            result.setMethod(cursor.getString(1));
            result.setFunction(cursor.getString(2));
            result.setNumbers(cursor.getString(3));
            result.setRepeat(cursor.getInt(4));
            result.setResult(cursor.getDouble(5));
            cursor.close();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to list tasks from the database", e);
        }
        return result;
    }
    public ArrayList<AproximationResult> list() {
        ArrayList<AproximationResult> results = new ArrayList<>();
        String queryStr = context.getString(R.string.list_aproximations_query);

        try {
            Cursor cursor = db.rawQuery(queryStr, new String[]{});

            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                AproximationResult result = new AproximationResult();

                result.setId(cursor.getLong(0));
                result.setMethod(cursor.getString(1));
                result.setFunction(cursor.getString(2));
                result.setNumbers(cursor.getString(3));
                result.setRepeat(cursor.getInt(4));
                result.setResult(cursor.getDouble(5));
                results.add(result);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to list aproximations from the database", e);
        }

        return results;
    }
    public boolean clear() {
        boolean status = true;
        String queryStr = context.getString(R.string.delete_aproximations_query);

        try {
            db.execSQL(queryStr);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to delete all entries from aproximations databse", e);
            status = false;
        }
        return status;
    }
}
