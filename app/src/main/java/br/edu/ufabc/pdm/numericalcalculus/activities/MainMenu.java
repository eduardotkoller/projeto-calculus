package br.edu.ufabc.pdm.numericalcalculus.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.singleton.AproximationsDao;
import br.edu.ufabc.pdm.numericalcalculus.singleton.IntegrationsDao;


public class MainMenu extends ActionBarActivity {

    private Spinner aproximacoes;
    private Spinner matrizes;
    private Spinner integracoes;
    private AproximationsDao aproximationsDao;
    private IntegrationsDao integrationsDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar)findViewById(R.id.action_bar);
        toolbar.setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        init();
        bindHandlers();
    }

    private void init() {
        aproximacoes = (Spinner)findViewById(R.id.spinner_aproximacoes);
        matrizes = (Spinner )findViewById(R.id.spinner_matrizes);
        integracoes = (Spinner )findViewById(R.id.spinner_integracoes);



        ArrayAdapter<CharSequence> aproximacoesAdapter = ArrayAdapter.createFromResource(this,
                R.array.aproximacoes_array, android.R.layout.simple_spinner_item);
        aproximacoesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> integracoesAdapter = ArrayAdapter.createFromResource(this,
                R.array.integracoes_array, android.R.layout.simple_spinner_item);
        integracoesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> matrizesAdapter = ArrayAdapter.createFromResource(this,
                R.array.matrizes_array, android.R.layout.simple_spinner_item);
        matrizesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        aproximacoes.setAdapter(aproximacoesAdapter);
        matrizes.setAdapter(matrizesAdapter);
        integracoes.setAdapter(integracoesAdapter);

        aproximationsDao = AproximationsDao.instantiate(this);
        integrationsDao = IntegrationsDao.instantiate(this);
    }

    private void bindHandlers() {
        final Context self = this;
        aproximacoes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (aproximacoes.getAdapter().getItem(position).toString().equals("Bisection")) {
                    Intent intent = new Intent(self, Bisection.class);
                    startActivity(intent);
                }
                if (aproximacoes.getAdapter().getItem(position).toString().equals("Fixed Point")) {

                        Intent intent = new Intent(self, FixedPoint.class);
                        startActivity(intent);

                }
                if (aproximacoes.getAdapter().getItem(position).toString().equals("Newton-Raphson")) {
                    Intent intent = new Intent(self, NewtonRaphson.class);
                    startActivity(intent);
                }
                if (aproximacoes.getAdapter().getItem(position).toString().equals("Secant")) {
                    Intent intent = new Intent(self, Secant.class);
                    startActivity(intent);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        matrizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (matrizes.getAdapter().getItem(position).toString().equals("Gauss")) {

                    Intent intent = new Intent(self, Gauss.class);
                    startActivity(intent);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        integracoes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (integracoes.getAdapter().getItem(position).toString().equals("MidPoint")) {

                    Intent intent = new Intent(self, MidPoint.class);
                    startActivity(intent);

                }
                if (integracoes.getAdapter().getItem(position).toString().equals("Trapeze")) {
                    Intent intent = new Intent(self, Trapeze.class);
                    startActivity(intent);
                }
                if (integracoes.getAdapter().getItem(position).toString().equals("Simpson")) {
                    Intent intent = new Intent(self, Simpson.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            String method = "mainMenu";
            Intent help = new Intent(this, About.class);
            help.putExtra("method", method);
            startActivity(help);
            return true;
        }
        if (id == R.id.action_history) {
            Intent history = new Intent(this, History.class);
            startActivity(history);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
