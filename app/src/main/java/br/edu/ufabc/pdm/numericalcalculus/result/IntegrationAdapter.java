package br.edu.ufabc.pdm.numericalcalculus.result;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.ufabc.pdm.numericalcalculus.R;
import br.edu.ufabc.pdm.numericalcalculus.singleton.IntegrationsDao;

/**
 * Created by eduardo on 30/04/15.
 */
public class IntegrationAdapter extends BaseAdapter {
    private IntegrationsDao dao;
    private ArrayList<IntegrationResult> results;
    private Context context;
    private LayoutInflater inflater;

    public IntegrationAdapter(Context c) {
        this.context = c;
        dao = IntegrationsDao.instantiate(context);
        results = dao.list();
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Object getItem(int position) {
        return results.get(position);
    }

    @Override
    public long getItemId(int position) {
        return results.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        TextView function;
        TextView method;
        if (convertView == null) {
            view = inflater.inflate(R.layout.history_item, null);
        } else
            view = convertView;

        IntegrationResult result = results.get(position);
        function = (TextView )view.findViewById(R.id.history_function);
        method = (TextView )view.findViewById(R.id.history_method);

        function.setText(result.getFunction());
        method.setText(result.getMethod());

        return view;
    }
}
